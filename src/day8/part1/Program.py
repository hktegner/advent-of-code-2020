from typing import List
from .Instruction import Instruction


class Program:

    def __init__(self, listing: List[str]):
        self._pc: int = 0
        self._acc: int = 0
        self._program = [Instruction(line) for line in listing]

    def run(self, initial_pc: int = 0, initial_acc: int = 0) -> (int, int):
        """
        Run the program and return the value of the accumulator
        when an instruction has been encountered (but not executed)
        for the second time, indicating an infinite loop. Or, the
        value of the accumulator at the end of the program.

        Returns the final program counter and accumulator value.
        """
        self._pc = initial_pc
        self._acc = initial_acc

        executed_line: List[bool] = [False for line in self._program]
        program_length: int = len(self._program)

        while self._pc < program_length and not executed_line[self._pc]:
            executed_line[self._pc] = True
            self._pc, self._acc = self._program[self._pc].run(self._pc, self._acc)

        return self._pc, self._acc
