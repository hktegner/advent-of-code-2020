
class Instruction:

    def __init__(self, line: str):
        bits = line.split()
        self._code = bits[0]
        self._operand = int(bits[1])

    def code(self) -> str:
        return self._code

    def operand(self) -> int:
        return self._operand

    def is_nop(self) -> bool:
        return self._code == "nop"

    def is_acc(self) -> bool:
        return self._code == "acc"

    def is_jmp(self) -> bool:
        return self._code == "jmp"

    def run(self, pc: int, acc: int) -> (int, int):
        """
        Run this instruction at program counter "pc" and
        with initial accumulator value "acc", and return
        tuplet (new pc, new acc).
        """
        if self.is_nop():
            return pc + 1, acc
        elif self.is_acc():
            return pc + 1, acc + self._operand
        else:  # must be jump
            return pc + self._operand, acc
