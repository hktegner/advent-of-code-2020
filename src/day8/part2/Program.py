from typing import List
from .Instruction import Instruction


class Program:

    def __init__(self, listing: List[str]):
        self._pc: int = 0
        self._acc: int = 0
        self._program = [Instruction(line) for line in listing]

    def jumps_or_nops(self):
        """
        A boolean array equal to length of program where each element
        indicates whether the instruction at that index is a jmp or nop (true),
        or otherwise false (acc).
        """
        return [ins.is_nop() or ins.is_jmp() for ins in self._program]

    def convert(self, line: int):
        line: Instruction = self._program[line]
        if line.is_nop():
            line.make_jmp()
        elif line.is_jmp():
            line.make_nop()
        else:
            raise RuntimeError(f"line {line} is neither nop or jmp")

    def run(self, initial_pc: int = 0, initial_acc: int = 0) -> (int, int):
        """
        Run the program and return the value of the accumulator
        when an instruction has been encountered (but not executed)
        for the second time, indicating an infinite loop. Or, the
        value of the accumulator at the end of the program.

        Returns the final program counter and accumulator value.
        """
        self._pc = initial_pc
        self._acc = initial_acc

        executed_line: List[bool] = [False for line in self._program]
        program_length: int = len(self._program)

        while self._pc < program_length and not executed_line[self._pc]:
            executed_line[self._pc] = True
            self._pc, self._acc = self._program[self._pc].run(self._pc, self._acc)

        return self._pc, self._acc
