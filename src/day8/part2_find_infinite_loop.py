from typing import List
from part2.Program import Program

with open("data/day8/program_text.txt", "r") as file:
    raw_lines = list(file)
    cleaned_lines = [line.strip() for line in raw_lines]

    p: Program = Program(cleaned_lines)
    candidate_lines: List[bool] = p.jumps_or_nops()
    program_length = len(candidate_lines)

    for candidate_line_index in range(program_length):

        # Skip acc operations
        if not candidate_lines[candidate_line_index]:
            continue

        print(f"Testing if line {candidate_line_index} ({cleaned_lines[candidate_line_index]}) is the bug")

        # Convert operation
        p.convert(candidate_line_index)

        pc: int = 0
        acc: int = 0
        pc, acc = p.run(pc, acc)

        if pc == program_length:
            # Found the bug
            print(f"Found the bug: line {candidate_line_index}")
            print(f"Final pc: {pc}")
            print(f"Final acc: {acc}")
            break
        else:
            print(f"Discarded {cleaned_lines[candidate_line_index]}")

        # Convert back
        p.convert(candidate_line_index)
