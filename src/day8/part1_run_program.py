from part1.Program import Program

with open("data/day8/program_text.txt", "r") as file:
    raw_lines = list(file)
    cleaned_lines = [line.strip() for line in raw_lines]

    print("First 5 lines")
    for line in cleaned_lines[:5]:
        print(line)

    print("Last 5 lines")
    for line in cleaned_lines[-5:]:
        print(line)

    p: Program = Program(cleaned_lines)
    pc: int = 0
    acc: int = 0
    pc, acc = p.run(pc, acc)

    print(f"Final pc = {pc}")
    print(f"Final acc = {acc}")
