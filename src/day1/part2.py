
import os
import sys
import itertools

sum_to_find: int = 2020
read_entries = []

with open("data/day1/part1_expense_entries.txt", "r") as data_file:
    entry: int = 0

    for line in data_file.readlines():
        entry = int(line.strip())
        print(f"Entry read: {entry}")
        read_entries.append(entry)

    print(f"Read {len(read_entries)} entries")

product: int = -1
for (x, y, z) in itertools.combinations(read_entries, 3):
    if (x + y + z == sum_to_find):
        product = x * y * z
        print(f"Found items adding up to required sum x = {x}, y = {y}, z = {z}, sum = {sum_to_find}, product = {product}")
        break

if (product == -1):
    print("Did not find the product :(")