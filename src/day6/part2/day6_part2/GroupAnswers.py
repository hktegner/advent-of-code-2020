from typing import List
from typing import Set

class GroupAnswers:

    def __init__(self, all_yesses: List[str]):
        """
        Create GroupAnswers object.
        Each line of the answers list is the answers from a single person.
        """
        self._all_yesses = all_yesses
        self._common_yesses = None
        for yesses in self._all_yesses:
            individual_yesses = set()
            for yes in yesses:
                individual_yesses.add(yes)

            if self._common_yesses is None:
                self._common_yesses = individual_yesses
            else:
                self._common_yesses = self._common_yesses & individual_yesses
        if self._common_yesses is None:
            self._common_yesses = set()

    def common_yesses(self) -> Set[str]:
        return self._common_yesses
        
