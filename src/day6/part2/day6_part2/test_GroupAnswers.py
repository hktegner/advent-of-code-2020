from typing import Set
from typing import List
import pytest
from .GroupAnswers import GroupAnswers

@pytest.mark.parametrize(
    "all_yesses, common_yesses",
    [
        ([], set()),
        (["a", "b", "c"], set()),
        (["a", "a", "a"], {"a"}),
        (["ab", "ab", "ab"], {"a", "b"}),
        (["abef", "cde", "abe"], {"e"}),
        (["", "abc", ""], set()),
    ]
)
def test_common_yesses(all_yesses: List[str], common_yesses: Set[str]):
    group = GroupAnswers(all_yesses)
    assert group.common_yesses() == common_yesses
