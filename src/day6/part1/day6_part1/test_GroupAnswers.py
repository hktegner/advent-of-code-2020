from typing import Set
from typing import List
import pytest
from .GroupAnswers import GroupAnswers

@pytest.mark.parametrize(
    "all_yesses, unique_yesses",
    [
        ([], set()),
        (["a", "b", "c"], {"a", "b", "c"}),
        (["a", "a", "a"], {"a"}),
        (["ab", "ab", "ab"], {"a", "b"}),
        (["abef", "cde", "ab"], {"a", "b", "c", "d", "e", "f"}),
        (["edf", "abc", ""], {"a", "b", "c", "d", "e", "f"}),
    ]
)
def test_unique_answers(all_yesses: List[str], unique_yesses: Set[str]):
    group = GroupAnswers(all_yesses)
    assert group.unique_answers() == unique_yesses
