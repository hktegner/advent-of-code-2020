from typing import List
from typing import Set

class GroupAnswers:

    def __init__(self, all_yesses: List[str]):
        """
        Create GroupAnswers object.
        Each line of the answers list is the answers from a single person.
        """
        self._all_yesses = all_yesses
        self._unique_yesses = set()
        for yesses in self._all_yesses:
            for yes in yesses:
                self._unique_yesses.add(yes)

    def unique_answers(self) -> Set[str]:
        return self._unique_yesses
        
