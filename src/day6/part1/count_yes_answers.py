from day6_part1.GroupAnswers import GroupAnswers

with open("data/day6/customs_form_answers.txt") as file:
    raw_answers = list(file)
    cleaned_answers = [answer.strip() for answer in raw_answers]

    group_answers = []

    yesses = 0
    groups = 0
    for index in range(len(cleaned_answers)):

        line = cleaned_answers[index]

        if line != "":
            group_answers.append(line)

        if line == "" or index == len(cleaned_answers)-1:
            group_yesses = GroupAnswers(group_answers)
            yesses = yesses + len(group_yesses.unique_answers())
            group_answers.clear()
            groups = groups + 1

    print(f"Lines processed: {len(raw_answers)}")
    print(f"Groups found: {groups}")
    print(f"Unique yesses across groups: {yesses}")
