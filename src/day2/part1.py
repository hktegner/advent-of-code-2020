
import os
import sys
import re

entry_pattern = re.compile(r"^(?P<min>\d+)\-(?P<max>\d+) (?P<character>.): (?P<pw>.*)$")

def is_valid_entry(entry: str) -> bool:
    """
    Validates whether this is a valid password file entry. Should follow the format:
    `x-y c: <password>`,
    where x = min number of occurences of c
          y = max number of occurrences of c
          c = character that must be in the password
    """
    if entry is None:
        return False

    match = entry_pattern.match(entry)
    if match is None:
        return False

    values = match.groupdict()

    print(f"found groups: {values}")

    if len(values.keys()) != 4:
        return False

    min: int = int(values['min'])
    max: int = int(values['max'])
    character: str = values['character']
    pw: str = values['pw']

    occurences = pw.count(character)

    return occurences >= min and occurences <= max

valid_passwords: int = 0
invalid_passwords: int = 0
entries_validated: int = 0

with open("data/day2/part1_password_list.txt", "r") as data_file:
    entry: int = 0

    for line in data_file.readlines():
        entry = line.strip()

        if is_valid_entry(entry):
            valid_passwords = valid_passwords + 1
            print(f"Valid: {entry}")
        else:
            invalid_passwords = invalid_passwords + 1
            print(f"Invalid: {entry}")

        entries_validated = entries_validated + 1

print(f"Total entries read: {entries_validated}")
print(f"Valid passwords: {valid_passwords}")
print(f"Invalid passwords: {invalid_passwords}")
