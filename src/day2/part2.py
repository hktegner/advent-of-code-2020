
import os
import sys
import re

entry_pattern = re.compile(r"^(?P<pos1>\d+)\-(?P<pos2>\d+) (?P<character>.): (?P<pw>.*)$")

def is_valid_entry(entry: str) -> bool:
    """
    Validates whether this is a valid password file entry. Should follow the format:
    `x-y c: <password>`,
    where x = check position 1, only one of x and y should be the character
          y = check position 2
          c = the character to check for
    """
    if entry is None:
        return False

    match = entry_pattern.match(entry)
    if match is None:
        return False

    values = match.groupdict()

    print(f"found groups: {values}")

    if len(values.keys()) != 4:
        return False

    pos1: int = int(values['pos1'])
    pos2: int = int(values['pos2'])
    character: str = values['character']
    pw: str = values['pw']
    pos1_char: str = pw[pos1-1]
    pos2_char: str = pw[pos2-1]
    matches: int = 0
    if pos1_char == character:
        matches = matches + 1
    if pos2_char == character:
        matches = matches + 1

    result: bool = (matches == 1)

    print(f"            11111111112")
    print(f"   12345678901234567890")
    print(f"pw={pw}, pos1={pos1}, pos2={pos2}, valid = {result}")

    return result

valid_passwords: int = 0
invalid_passwords: int = 0
entries_validated: int = 0

with open("data/day2/part1_password_list.txt", "r") as data_file:
    entry: int = 0

    for line in data_file.readlines():
        entry = line.strip()

        print(f"Entry read: {entry}")

        if is_valid_entry(entry):
            valid_passwords = valid_passwords + 1
            print(f"Valid: {entry}")
        else:
            invalid_passwords = invalid_passwords + 1
            print(f"Invalid: {entry}")

        entries_validated = entries_validated + 1

print(f"Total entries read: {entries_validated}")
print(f"Valid passwords: {valid_passwords}")
print(f"Invalid passwords: {invalid_passwords}")
