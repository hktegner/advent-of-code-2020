

class Seat:

    def __init__(self, seat_code: str, row: int=0, col: int=0, row_size: int=8):
        self._row_size = row_size
        if seat_code is None or len(seat_code) == 0:
            self._seat_code = "not given"
            self._row_number = row
            self._column_number = col
            self._seat_id = self._row_number * self._row_size + self._column_number
        else:
            self._check_valid_seat_code(seat_code)
            self._seat_code = seat_code

            self._row_number = self._find_row_number(seat_code)
            self._column_number = self._find_col_number(seat_code)
            self._seat_id = self._row_number * self._row_size + self._column_number

    def seat_id(self) -> int:
        return self._seat_id

    def row_number(self) -> int:
        return self._row_number

    def col_number(self) -> int:
        return self._column_number

    def _find_row_number(self, seat_code: str) -> int:
        row_code: str = seat_code[0:7]
        row_code = row_code.replace("F", "0")
        row_code = row_code.replace("B", "1")
        
        return int(row_code, 2)
   
    def _find_col_number(self, seat_code: str) -> int:
        col_code = seat_code[7:]
        col_code = col_code.replace("L", "0")
        col_code = col_code.replace("R", "1")

        return int(col_code, 2)

    def _check_valid_seat_code(self, seat_code: str) -> None:
        if len(seat_code) != 10:
            raise RuntimeError(f"Invalid seat code {seat_code}. Must be 10 chars.")

    def __str__(self):
        return "row={row}, col={col}, id={id}".format(row=self._row_number, col=self._column_number, id=self._seat_id)