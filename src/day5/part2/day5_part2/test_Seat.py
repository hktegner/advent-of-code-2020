import pytest
from .Seat import Seat

@pytest.mark.parametrize(
    "seat_code, expected_seat_id",
    [
        ("FBFBBFFRLR", 357),
        ("BFFFBBFRRR", 567),
        ("FFFBBBFRRR", 119),
        ("BBFFBBFRLL", 820)
    ]
)
def test_seat_code_to_id(seat_code: str, expected_seat_id: int):
    assert expected_seat_id == Seat(seat_code).seat_id()

@pytest.mark.parametrize(
    "row_size, seat, expected_id",
    [
        (4, Seat(None, row=0, col=0, row_size=4), 0),
        (4, Seat(None, row=0, col=1, row_size=4), 1),
        (4, Seat(None, row=0, col=2, row_size=4), 2),
        (4, Seat(None, row=0, col=3, row_size=4), 3),
        (4, Seat(None, row=1, col=0, row_size=4), 4),
        (8, Seat(None, row=127, col=7, row_size=8), 1023)
    ]
)
def test_different_row_size(row_size:int, seat: Seat, expected_id: int):
    assert seat.seat_id() == expected_id
