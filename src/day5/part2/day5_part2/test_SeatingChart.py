
from .Seat import Seat
from .SeatingChart import SeatingChart

def test_is_allocated():
    chart = SeatingChart(rows=2, cols=5)
    seat02 = Seat(None, 0, 2, row_size=5)
    seat03 = Seat(None, 0, 3, row_size=5)
    seat04 = Seat(None, 0, 4, row_size=5)
    seat10 = Seat(None, 1, 0, row_size=5)
    seat11 = Seat(None, 1, 1, row_size=5)

    assert chart.allocate(seat02)
    assert chart.allocate(seat03)
    assert chart.allocate(seat04)
    assert chart.allocate(seat10)
    assert chart.allocate(seat11)

    # check allocated seats

    assert chart.is_allocated(seat02)
    assert chart.is_allocated(seat03)
    assert chart.is_allocated(seat04)
    assert chart.is_allocated(seat10)
    assert chart.is_allocated(seat11)

    # check unallocated seats

    assert not chart.is_allocated(Seat(None, 0, 0, row_size=5))
    assert not chart.is_allocated(Seat(None, 0, 1, row_size=5))
    assert not chart.is_allocated(Seat(None, 1, 2, row_size=5))
    assert not chart.is_allocated(Seat(None, 1, 3, row_size=5))
    assert not chart.is_allocated(Seat(None, 1, 4, row_size=5))