from .Seat import Seat
from typing import List

class SeatingChart:

    def __init__(self, rows: int, cols: int):
        self._seat_matrix = [[None for c in range(cols)] for r in range(rows)]

    def allocate(self, seat: Seat) -> bool:
        """
        Allocate the seat in the chart.
        Returns true if allocated successfully, false is already allocated.
        """
        if self.is_allocated(seat):
            return False
        else:
            self._seat_matrix[seat.row_number()][seat.col_number()] = seat
            return True
        
    def is_allocated(self, seat: Seat) -> bool:
        return not self._seat_matrix[seat.row_number()][seat.col_number()] is None
    
    def find_empty(self) -> List[Seat]:
        empty_seats = []
        rows = len(self._seat_matrix)
        cols = len(self._seat_matrix[0])
        for row in range(rows):
            for col in range(cols):
                if self._seat_matrix[row][col] is None:
                    empty_seats.append(Seat(seat_code=None, row=row, col=col))
        
        return empty_seats