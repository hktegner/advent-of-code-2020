from day5_part2.Seat import Seat
from day5_part2.SeatingChart import SeatingChart

with open("data/day5/boarding_passes.txt", "r") as file:

    chart = SeatingChart(rows=128, cols=8)

    read_passports = list(file)
    passports = [passport.strip() for passport in read_passports]

    print(f"Passports read: {len(passports)}")

    for passport in passports:
        seat = Seat(passport)
        chart.allocate(seat)

    unallocated = chart.find_empty()
    print(f"Found unallocated seats: {len(unallocated)}")
    for free_seat in unallocated:
        print(f"Found free seat: {free_seat}")
