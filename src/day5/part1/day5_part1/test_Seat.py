import pytest
from .Seat import Seat

@pytest.mark.parametrize(
    "seat_code, expected_seat_id",
    [
        ("FBFBBFFRLR", 357),
        ("BFFFBBFRRR", 567),
        ("FFFBBBFRRR", 119),
        ("BBFFBBFRLL", 820)
    ]
)
def test_seat_code_to_id(seat_code: str, expected_seat_id: int):
    assert expected_seat_id == Seat(seat_code).seat_id()
