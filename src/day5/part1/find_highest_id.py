from day5_part1.Seat import Seat

with open("data/day5/boarding_passes.txt", "r") as file:
    passports = list(file)
    passports = [passport.strip() for passport in passports]

    highest_id: int = -1
    for passport in passports:
        seat = Seat(passport)
        if seat.seat_id() > highest_id:
            highest_id = seat.seat_id()
        
    print(f"Passports inspected: {len(passports)}")
    print(f"Highest passport id: {highest_id}")
