"""
This is actually part 2.
"""
from part1.Rules import Rules

with open('data/day7/luggage_rules.txt', 'r') as file:
    definitions = list(file)
    clean_defs = [definition.strip() for definition in definitions]
    rules: Rules = Rules(clean_defs)
    bags_required: int = rules.find_number_of_bags_required('shiny gold')
    print(f"Definitions read: {len(clean_defs)}")
    print(f"Bags required for shiny gold: {bags_required}")
