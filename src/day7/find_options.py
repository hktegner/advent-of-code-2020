from part1.Rules import Rules

with open('data/day7/luggage_rules.txt', 'r') as file:
    definitions = list(file)
    clean_defs = [definition.strip() for definition in definitions]
    rules: Rules = Rules(clean_defs)
    found_styles = rules.find_bags_containing('shiny gold')
    print(f"Found {len(found_styles)} styles")
    print(f"Styles found: {found_styles}")
