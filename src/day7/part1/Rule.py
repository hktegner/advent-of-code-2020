from typing import List
from typing import Tuple
import re


class Rule:

    def __init__(self, definition: str):
        def_pattern = re.compile(r'(\w+ \w+) bags contain (.*).')
        match = def_pattern.match(definition)
        if match is None:
            raise RuntimeError(f"Unable to parse rule: {definition}")

        self._bag_style = match.group(1)
        self._can_contain: List[Tuple] = []

        contents = match.group(2)
        if contents != "no other bags":
            for part in contents.split(", "):
                content_pattern = re.compile(r'^(\d+) (\w+ \w+) (bag|bags)$')
                match = content_pattern.match(part)

                if match is None:
                    raise RuntimeError(f"Unable to parse content part definition: {part}")

                count = int(match.group(1))
                style = match.group(2)

                self._can_contain.append((count, style))

    def style(self) -> str:
        return self._bag_style

    def contents(self) -> List[Tuple]:
        """
        Returns a list of tuples describing counts and styles of bags
        that can be contained within this one (bag count, style).
        """
        return self._can_contain
