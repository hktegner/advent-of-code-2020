from typing import List
from typing import Dict
from .Rule import Rule


class Rules:

    def __init__(self, rule_definitions: List[str]):
        self._rules_list: List[Rule] = [Rule(definition) for definition in rule_definitions]
        self._rules_dict: Dict = {}
        for rule in self._rules_list:
            self._rules_dict[rule.style()] = rule

    def rules(self) -> List[Rule]:
        return self._rules_list

    def find_number_of_bags_required(self, required_style) -> int:
        if required_style not in self._rules_dict:
            raise RuntimeError(f"Root Rule not found for {required_style}")

        root_rule: Rule = self._rules_dict[required_style]
        return self._number_of_bags_inside(root_rule)

    def _number_of_bags_inside(self, rule: Rule) -> int:
        bags_inside: int = 0
        for (content_count, content_style) in rule.contents():
            content_rule = self._rules_dict[content_style]
            bags_inside += content_count + content_count * self._number_of_bags_inside(content_rule)
        return bags_inside

    def find_bags_containing(self, required_style: str) -> List[str]:
        """
        Return style of bags that can hold the given style of bag.
        F.x. find_bags_containing('shiny gold') could return
        ['bright white', 'dotted black'] if they both could either
        contain a 'shiny gold' bag, or contain a different style
        of bag that again could contain a 'shiny gold' bag.

        :param required_style: str = style of bag to find
        :return: array of top-level styles that could somehow in
        their contaiment hierarchy contain the given style
        """
        found_bags: List[str] = []
        for rule in self._rules_list:
            if self._rule_can_contain(rule, required_style):
                found_bags.append(rule.style())

        return found_bags

    def _rule_can_contain(self, rule: Rule, required_style: str) -> bool:
        for (content_count, content_style) in rule.contents():
            if content_style == required_style:
                return True
            else:
                content_rule = self._rules_dict[content_style]
                if content_rule is not None and self._rule_can_contain(content_rule, required_style):
                    return True

        return False
