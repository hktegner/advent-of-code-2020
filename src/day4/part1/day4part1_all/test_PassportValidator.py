from .PassportValidator import PassportValidator

class TestPassportValidator:

    def test_truly_valid(self):
        assert PassportValidator().is_valid(dict(
            byr = "1920",
            iyr = "2020",
            eyr = "2050",
            hgt = "192cm",
            hcl = "black",
            ecl = "brown",
            pid = "10293-29392",
            cid = "norway"
        ))

    def test_valid_northpole(self):
        assert PassportValidator().is_valid(dict(
            byr = "1920",
            iyr = "2020",
            eyr = "2050",
            hgt = "192cm",
            hcl = "black",
            ecl = "brown",
            pid = "10293-29392"
        ))

    def test_invalid(self):
        assert not PassportValidator().is_valid({})
