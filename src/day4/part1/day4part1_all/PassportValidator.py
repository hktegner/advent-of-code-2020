from typing import List

class PassportValidator:

    BIRTH_YEAR = "byr"
    ISSUE_YEAR = "iyr"
    EXPIRATION_YEAR = "eyr"
    HEIGHT = "hgt"
    HAIR_COLOUR = "hcl"
    EYE_COLOUR = "ecl"
    PASSPORT_ID = "pid"
    COUNTRY_ID = "cid"

    def is_valid(self, data: dict) -> bool:
        return self.BIRTH_YEAR in data and \
            self.ISSUE_YEAR in data and \
                self.EXPIRATION_YEAR in data and \
                    self.HEIGHT in data and \
                        self.HAIR_COLOUR in data and \
                            self.EYE_COLOUR in data and \
                                self.PASSPORT_ID in data
