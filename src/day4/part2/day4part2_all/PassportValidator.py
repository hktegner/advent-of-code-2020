from typing import List
import re

class PassportValidator:

    BIRTH_YEAR = "byr"
    ISSUE_YEAR = "iyr"
    EXPIRATION_YEAR = "eyr"
    HEIGHT = "hgt"
    HAIR_COLOUR = "hcl"
    EYE_COLOUR = "ecl"
    PASSPORT_ID = "pid"
    COUNTRY_ID = "cid"

    def is_valid(self, data: dict) -> bool:
        return self.is_valid_birth_year(data) and \
            self.is_valid_issue_year(data) and \
                self.is_valid_expiration_year(data) and \
                    self.is_valid_height(data) and \
                        self.is_valid_hair_colour(data) and \
                            self.is_valid_eye_colour(data) and \
                                self.is_valid_passport_id(data) and \
                                    self.is_valid_country_code(data)

    def is_valid_birth_year(self, data: dict) -> bool:
        if not self.BIRTH_YEAR in data:
            return False
        year: str = data[self.BIRTH_YEAR]
        if len(year) != 4:
            return False
        year_number: int = int(year)
        return 1920 <= year_number <= 2002

    def is_valid_issue_year(self, data: dict) -> bool:
        if not self.ISSUE_YEAR in data:
            return False
        year: str = data[self.ISSUE_YEAR]
        if len(year) != 4:
            return False
        year_number: int = int(year)
        return 2010 <= year_number <= 2020

    def is_valid_expiration_year(self, data: dict) -> bool:
        if not self.EXPIRATION_YEAR in data:
            return False
        year: str = data[self.EXPIRATION_YEAR]
        if len(year) != 4:
            return False
        year_number: int = int(year)
        return 2020 <= year_number <= 2030

    def is_valid_height(self, data: dict) -> bool:
        if not self.HEIGHT in data:
            return False
        height: str = data[self.HEIGHT]

        p = re.compile("(?P<value>[0-9]*)(?P<unit>in|cm)")
        m = p.match(height)
        
        if m is None:
            return False

        if len(m.groupdict().keys()) != 2:
            return False

        value: int = int(m.group(1))
        unit: str = m.group(2)

        if unit == "in":
            return 59 <= value <= 76
        else: # cm
            return 150 <= value <= 193

    def is_valid_hair_colour(self, data: dict) -> bool:
        if not self.HAIR_COLOUR in data:
            return False
        colour = data[self.HAIR_COLOUR]
        p = re.compile(r"#[0-9a-f]{6}")
        m = p.match(colour)
        return not m is None

    def is_valid_eye_colour(self, data: dict) -> bool:
        if not self.EYE_COLOUR in data:
            return False
        colour: str = data[self.EYE_COLOUR]
        return colour in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

    def is_valid_passport_id(self, data: dict) -> bool:
        if not self.PASSPORT_ID in data:
            return False
        id = data[self.PASSPORT_ID]

        p = re.compile(r"^[0-9]{9}$")
        m = p.match(id)
        return not m is None

    def is_valid_country_code(self, data: dict) -> bool:
        # Always true for now
        return True

