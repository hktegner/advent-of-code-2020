import pytest
from .PassportValidator import PassportValidator

class TestPassportValidator:

    VALID_DETAILS: dict = dict(
        byr = "1920",
        iyr = "2020",
        eyr = "2030",
        hgt = "150cm",
        hcl = "#000000",
        ecl = "amb",
        pid = "012345678",
        cid = "ignored"
    )

    VALID_NORTHPOLE_DETAILS: dict = dict(
        byr = "1920",
        iyr = "2020",
        eyr = "2030",
        hgt = "150cm",
        hcl = "#000000",
        ecl = "amb",
        pid = "012345678",
        cid = "ignored"
    )

    def test_truly_valid(self):
        assert PassportValidator().is_valid(self.VALID_DETAILS)

    def test_valid_northpole(self):
        assert PassportValidator().is_valid(self.VALID_NORTHPOLE_DETAILS)

    @pytest.mark.parametrize(
        "year, expect_valid",
        [
            ("1919", False),
            ("1920", True),
            ("1985", True),
            ("2002", True),
            ("2003", False),
        ]
    )
    def test_birthyear_validation(self, year: str, expect_valid: bool):
        data: dict = self.VALID_DETAILS.copy()
        data[PassportValidator.BIRTH_YEAR] = year
        valid_passport: bool = PassportValidator().is_valid(data)
        if expect_valid:
            assert valid_passport
        else:
            assert not valid_passport

    @pytest.mark.parametrize(
        "year, expect_valid",
        [
            ("2009", False),
            ("2010", True),
            ("2015", True),
            ("2020", True),
            ("2021", False),
        ]
    )
    def test_issueyear_validation(self, year: str, expect_valid: bool):
        data: dict = self.VALID_DETAILS.copy()
        data[PassportValidator.ISSUE_YEAR] = year
        valid_passport: bool = PassportValidator().is_valid(data)
        if expect_valid:
            assert valid_passport
        else:
            assert not valid_passport

    @pytest.mark.parametrize(
        "year, expect_valid",
        [
            ("2019", False),
            ("2020", True),
            ("2022", True),
            ("2030", True),
            ("2031", False),
        ]
    )
    def test_expirationyear_validation(self, year: str, expect_valid: bool):
        data: dict = self.VALID_DETAILS.copy()
        data[PassportValidator.EXPIRATION_YEAR] = year
        valid_passport: bool = PassportValidator().is_valid(data)
        if expect_valid:
            assert valid_passport
        else:
            assert not valid_passport

    @pytest.mark.parametrize(
        "height, expect_valid",
        [
            ("1.45m", False),
            ("66m", False),
            ("1500mm", False),
            ("149cm", False),
            ("150cm", True),
            ("193cm", True),
            ("194cm", False),
            ("58in", False),
            ("59in", True),
            ("76in", True),
            ("77in", False)
        ]
    )
    def test_height_validation(self, height: str, expect_valid: bool):
        data: dict = self.VALID_DETAILS.copy()
        data[PassportValidator.HEIGHT] = height
        valid_passport: bool = PassportValidator().is_valid(data)
        if expect_valid:
            assert valid_passport
        else:
            assert not valid_passport

    @pytest.mark.parametrize(
        "colour, expect_valid",
        [
            ("red", False),
            ("#abc", False),
            ("aabbff", False),
            ("#fffffg", False),
            ("#888888", True),
            ("#aabbcc", True),
            ("#a77b9c", True)
        ]
    )
    def test_hairrcolour_validation(self, colour: str, expect_valid: bool):
        data: dict = self.VALID_DETAILS.copy()
        data[PassportValidator.HAIR_COLOUR] = colour
        valid_passport: bool = PassportValidator().is_valid(data)
        if expect_valid:
            assert valid_passport
        else:
            assert not valid_passport

    @pytest.mark.parametrize(
        "colour, expect_valid",
        [
            ("red", False),
            ("black", False),
            ("#ff00bb", False),
            ("light-blue", False),
            ("amb", True),
            ("blu", True),
            ("gry", True),
            ("grn", True),
            ("hzl", True),
            ("oth", True)
        ]
    )
    def test_eyecolour_validation(self, colour: str, expect_valid: bool):
        data: dict = self.VALID_DETAILS.copy()
        data[PassportValidator.EYE_COLOUR] = colour
        valid_passport: bool = PassportValidator().is_valid(data)
        if expect_valid:
            assert valid_passport
        else:
            assert not valid_passport

    @pytest.mark.parametrize(
        "id, expect_valid",
        [
            ("abcdf", False),
            ("12345678", False),
            ("123-123-f", False),
            ("123456789", True),
            ("007555123", True)
        ]
    )
    def test_passportid_validation(self, id: str, expect_valid: bool):
        data: dict = self.VALID_DETAILS.copy()
        data[PassportValidator.PASSPORT_ID] = id
        valid_passport: bool = PassportValidator().is_valid(data)
        if expect_valid:
            assert valid_passport
        else:
            assert not valid_passport

    def test_empty_is_invalid(self):
        assert not PassportValidator().is_valid({})
