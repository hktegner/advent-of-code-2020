from day4part2_all.PassportValidator import PassportValidator

validator = PassportValidator()

with open("data/day4/part1_passport_list.txt", "r") as file:
    lines = list(file)
    lines = [line.strip() for line in lines]

    passport_props = dict()
    valid_passports = 0
    invalid_passports = 0
    for line_index in range(len(lines)):
        line = lines[line_index]

        if len(line) > 0:
            chunks = line.split(" ")
            passport_props.update(dict(chunk.split(":") for chunk in chunks))

        if len(line) == 0 or line_index == len(lines)-1:
            if len(passport_props.keys()) > 0:
                valid = validator.is_valid(passport_props)
                print(f"passport({passport_props}) valid={valid}")
                passport_props = dict()
                if valid:
                    valid_passports = valid_passports + 1
                else:
                    invalid_passports = invalid_passports + 1

        
    print(f"Valid passports = {valid_passports}")
    print(f"Invalid passports = {invalid_passports}")
    print(f"Total passports read = {valid_passports+invalid_passports}")