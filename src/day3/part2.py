from day3_all.Terrain import Terrain
from day3_all.Toboggan import Toboggan

with open("data/day3/part1_treemap.txt", "r") as file:
    lines = list(file)
    terrain = Terrain(lines)
    toboggan = Toboggan()

    trees1: int = toboggan.trace_route(1, 1, terrain)
    print(f"Run 1,1 encounters {trees1} trees")

    trees2: int = toboggan.trace_route(3, 1, terrain)
    print(f"Run 3,1 encounters {trees2} trees")

    trees3: int = toboggan.trace_route(5, 1, terrain)
    print(f"Run 5,1 encounters {trees3} trees")

    trees4: int = toboggan.trace_route(7, 1, terrain)
    print(f"Run 7,1 encounters {trees4} trees")

    trees5: int = toboggan.trace_route(1, 2, terrain)
    print(f"Run 1,2 encounters {trees5} trees")

    product = trees1 * trees2 * trees3 * trees4 * trees5
    print(f"Product of run1 * run2 * run3 * run4 * run5 = {product}")