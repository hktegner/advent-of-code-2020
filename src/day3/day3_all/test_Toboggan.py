from .Terrain import Terrain
from .Toboggan import Toboggan

class TestToboggan:

    def test_simple(self):
        terrain = Terrain([
            "#.",
            ".#"
        ])
        toboggan = Toboggan()
        assert toboggan.trace_route(1, 1, terrain) == 2

    