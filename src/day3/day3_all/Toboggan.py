from typing import List
from . import Terrain

class Toboggan:
    """
    The class that calculates number of trees hit for a certain trajectory
    """

    def trace_route(self, delta_x: int, delta_y: int, terrain: Terrain) -> int:
        """
        Trace a route down the hill, starting at position (col=0, row=0)
        and 'delta_y' rows down and 'delta_x' horizontally. Returning 
        the number of trees hit on the way down. At least one of 'delta_x' 
        or 'delta_y' must be 1.
        """
        if (not delta_x == 1) and (not delta_y == 1):
            raise RuntimeError(f"Got dx={delta_x} and dy={delta_y}. At least one must be 1.")

        x: int = 0
        y: int = 0
        trees: int = 0
        height: int = terrain.raw_height()

        while y < height:
            if terrain.is_tree(x, y):
                trees = trees + 1
            x = x + delta_x
            y = y + delta_y

        return trees

