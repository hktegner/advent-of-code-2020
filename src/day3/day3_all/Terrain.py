from typing import List

class Terrain:

    top_down_repeating_map: List[str]
    width: int = 0
    height: int = 0

    def __init__(self, raw_lines: List[str]):
        self.top_down_repeating_map = [line.strip() for line in raw_lines]
        for line in self.top_down_repeating_map:
            for x in line:
                if x not in ["#", "."]:
                    raise RuntimeError(f"Found invalid character {x}. Only # and . are valid characters.")
        self.width = 0 if len(self.top_down_repeating_map) == 0 else len(self.top_down_repeating_map[0])
        self.height = len(self.top_down_repeating_map)
    
    def raw_width(self) -> int:
        return self.width

    def raw_height(self) -> int:
        return self.height
        
    def is_tree(self, x: int, y: int) -> bool:
        """
        Check if there is a tree at the given coordinates on the map.
        Raises a RuntimeError if the y-coordinate is invalid.

        Parameters:
        x = 0-based horizontal position, can be negative
        y = 0-based vertical position, cannot be negative and must be within 0 to map height - 1
        """
        if y < 0:
            raise RuntimeError(f"y={y} < 1")
        if y >= self.height:
            raise RuntimeError(f"y={y} >= height={self.height}")
        effective_x = x % self.width

        return self.top_down_repeating_map[y][effective_x] == '#'

    def is_open(self, x: int, y: int) -> bool:
        return not self.is_tree(x, y)
