import pytest
from .Terrain import Terrain

class TestTerrain:

    def test_rejects_invalid_map(self):
        with pytest.raises(RuntimeError):
            Terrain([
                "x"
            ])
        with pytest.raises(RuntimeError):
            Terrain([
                "#..f"
            ])

    def test_dimensions(self):
        t = Terrain([
            "#..#...",
            "..#..#."
        ])
        assert t.raw_width() == 7
        assert t.raw_height() == 2

    def test_is_tree_invalid_pos(self):
        t = Terrain([
            "#.",
            ".#",
            "##"
        ])
        with pytest.raises(RuntimeError):
            t.is_tree(0, -1)
        with pytest.raises(RuntimeError):
            t.is_tree(0, 3)
        assert t.is_tree(5000, 2)

    def test_is_tree_valid_pos(self):
        t = Terrain([
            "#.",
            ".#",
            "##"
        ])
        assert t.is_tree(0, 0)
        assert t.is_open(1, 0)
        assert t.is_tree(2, 0)
        assert t.is_open(3, 0)
        assert t.is_open(0, 1)
        assert t.is_tree(1, 1)
        assert t.is_tree(0, 2)
        assert t.is_tree(1, 2)

