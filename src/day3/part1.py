from day3_all.Terrain import Terrain
from day3_all.Toboggan import Toboggan

with open("data/day3/part1_treemap.txt", "r") as file:
    lines = list(file)
    terrain = Terrain(lines)
    toboggan = Toboggan()
    trees: int = toboggan.trace_route(3, 1, terrain)
    print(f"Run 3,1 encounters {trees} trees")

