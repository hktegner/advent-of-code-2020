import pytest
from day8.part2.Instruction import Instruction


def test_valid_conversion():
    i = Instruction("jmp +2")
    i.make_nop()

    assert i.is_nop()

    i.make_jmp()

    assert i.is_jmp()


def test_invalid_conversion():
    i = Instruction("acc +5")

    with pytest.raises(RuntimeError) as exc_info:
        i.make_jmp()

    assert exc_info is not None

    with pytest.raises(RuntimeError) as exc_info:
        i.make_nop()

    assert exc_info is not None
