import pytest
from day8.part1.Instruction import Instruction

@pytest.mark.parametrize(
    "line, expected_code, expected_operand",
    [
        ('nop +0',  'nop', 0),
        ('acc +1',  'acc', 1),
        ('jmp +4',  'jmp', 4),
        ('jmp -4',  'jmp', -4)
    ]
)
def test_construction(line: str, expected_code: str, expected_operand: int):
    i = Instruction(line)
    assert i.code() == expected_code
    assert i.operand() == expected_operand

@pytest.mark.parametrize(
    "line, initial_pc, initial_acc, final_pc, final_acc",
    [
        ('nop +0',  5, 3, 6, 3),
        ('acc +1',  0, 1, 1, 2),
        ('jmp +4',  4, 0, 8, 0),
        ('jmp -4',  10, 0, 6, 0)
    ]
)
def test_run(line: str, initial_pc: int, initial_acc: int, final_pc: int, final_acc: int):
    i = Instruction(line)
    actual_final_pc, actual_final_acc = i.run(initial_pc, initial_acc)
    assert final_pc == actual_final_pc
    assert final_acc == actual_final_acc
