from typing import List
import pytest
from day7.part1.Rules import Rules


@pytest.mark.parametrize(
    'required_style, expected_containing_styles',
    [('shiny gold', ['bright white', 'muted yellow', 'dark orange', 'light red'])]
)
def test_rules_recursion(required_style, expected_containing_styles: List[str]):
    rules: Rules = Rules([
        'light red bags contain 1 bright white bag, 2 muted yellow bags.',
        'dark orange bags contain 3 bright white bags, 4 muted yellow bags.',
        'bright white bags contain 1 shiny gold bag.',
        'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.',
        'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
        'dark olive bags contain 3 faded blue bags, 4 dotted black bags.',
        'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.',
        'faded blue bags contain no other bags.',
        'dotted black bags contain no other bags.'
    ])
    found_containing_styles: List[str] = rules.find_bags_containing(required_style)
    found_containing_styles.sort()
    expected_containing_styles.sort()
    assert found_containing_styles == expected_containing_styles


@pytest.mark.parametrize(
    'required_style, expected_bags_contained',
    [('shiny gold', 126)]
)
def test_bag_count(required_style, expected_bags_contained: int):
    rules: Rules = Rules([
        'shiny gold bags contain 2 dark red bags.',
        'dark red bags contain 2 dark orange bags.',
        'dark orange bags contain 2 dark yellow bags.',
        'dark yellow bags contain 2 dark green bags.',
        'dark green bags contain 2 dark blue bags.',
        'dark blue bags contain 2 dark violet bags.',
        'dark violet bags contain no other bags.'
    ])
    assert rules.find_number_of_bags_required(required_style) == expected_bags_contained
